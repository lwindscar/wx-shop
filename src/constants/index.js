export const LOGIN_MUTATION = `
  mutation login($phoneNumber: String!, $password: String!) {
    login(phoneNumber: $phoneNumber, password: $password) {
      token
      user {
        id
        name
      }
    }
  }
`;

export const CART_ITEM_QUERY = `
  query {
    cartItems {
      id
      quantity
      isCheckout
      customer {
        id
        name
      }
      product {
        id
        name
        price
      }
    }
  }
`;
