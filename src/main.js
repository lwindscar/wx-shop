import Vue from 'vue';
import App from './App';
// import { baseTextColor, baseActiveTextColor } from './utils/theme';
// import store from './store';

Vue.config.productionTip = false;
App.mpType = 'app';

// Vue.prototype.$store = store;

const app = new Vue(App);
app.$mount();

export default {
  // 这个字段走 app.json
  config: {
    // 页面前带有 ^ 符号的，会被编译成首页，其他页面可以选填，我们会自动把 webpack entry 里面的入口页面加进去
    pages: [
      // '^pages/logs/main',
      'pages/index/main',
      'pages/category/main',
      'pages/cart/main',
      'pages/mine/main',
      'pages/product/main',
    ],
    window: {
      navigationBarBackgroundColor: '#fff',
      navigationBarTextStyle: 'black',
      navigationBarTitleText: 'z-shop',
      navigationStyle: 'custom',
      debug: true,
    },
    tabBar: {
      list: [
        {
          pagePath: 'pages/index/main',
          text: '首页',
          iconPath: '/static/images/home-icon.png',
          selectedIconPath: '/static/images/home-selected-icon.png',
        },
        {
          pagePath: 'pages/category/main',
          // pagePath: 'pages/product/main',
          text: '分类',
          iconPath: '/static/images/home-icon.png',
          selectedIconPath: '/static/images/home-selected-icon.png',
        },
        {
          pagePath: 'pages/cart/main',
          text: '购物车',
          iconPath: '/static/images/cart-icon.png',
          selectedIconPath: '/static/images/cart-selected-icon.png',
        },
        {
          pagePath: 'pages/mine/main',
          text: '个人',
          iconPath: '/static/images/home-icon.png',
          selectedIconPath: '/static/images/home-selected-icon.png',
        },
      ],
    },
    color: '#666666',
    selectedColor: '#4d3126',
    backgroundColor: '#fafafa',
    borderStyle: 'white',
  },
};
