// https://vuex.vuejs.org/zh-cn/intro.html
// make sure to call Vue.use(Vuex) if using a module system
import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

const persistPlugin = createPersistedState({
  storage: {
    getItem: wx.getStorageSync,
    setItem: wx.setStorageSync,
    // removeItem: wx.removeStorageSync,
    removeItem: () => {},
  },
});

const store = new Vuex.Store({
  plugins: [persistPlugin],
  state: {
    count: 0,
  },
  getters: {},
  mutations: {
    increment: (state) => {
      const obj = state;
      obj.count += 1;
    },
    decrement: (state) => {
      const obj = state;
      obj.count -= 1;
    },
  },
  actions: {},
});

export default store;
