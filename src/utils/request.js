export const API_ENDPOINT = 'http://localhost:4000';

export const DB_ENDPOINT = 'http://localhost:4466';
const DB_TOKEN =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InNlcnZpY2UiOiJkZWZhdWx0QGRlZmF1bHQiLCJyb2xlcyI6WyJhZG1pbiJdfSwiaWF0IjoxNTI1NTk5NTgwLCJleHAiOjE1MjYyMDQzODB9.4sVtKLISkgW1TtJ10KF5heQQPnehzWAB-9gFnsG67_0';

const request = (options, query, variables, operationName) =>
  new Promise((resolve, reject) => {
    let data = {};
    if (query) data.query = query;
    if (variables) data.variables = variables;
    if (operationName) data.operationName = operationName;
    data = JSON.stringify(data);

    setTimeout(() => {
      wx.request({
        data,
        method: 'POST',
        success(res) {
          const { data: d, ...rest } = res;
          if (d.errors) {
            reject({ errors: d.errors, ...rest });
          } else {
            resolve({ data: d.data, ...rest });
          }
        },
        fail: reject,
        ...options,
      });
    }, 2000);
  });

export const apiRequest = (query, variables, operationName) => {
  const apiToken = wx.getStorageSync('wx-token');
  const header = apiToken ? { Authorization: `Bearer ${apiToken}` } : {};

  return request(
    { url: API_ENDPOINT, header },
    query,
    variables,
    operationName,
  );
};

export const dbRequest = (query, variables, operationName) => {
  const header = { Authorization: `Bearer ${DB_TOKEN}` };

  return request({ url: DB_ENDPOINT, header }, query, variables, operationName);
};

export default request;
